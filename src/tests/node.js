var Distilled = require('distilled-distilled');
var assert = require('assert');

var suite = new Distilled();

var execSync = require('child_process').execSync;

suite.test('Reporter', function () {
    this.test('Basic', function () {

        var sample = (function () {
            var Distilled = require('distilled-distilled');
            var reporter = require('reporter');
            var sample = new Distilled(reporter);

            sample.test('A', true).test('a', false).test('_a', false);
            sample.test('B', true).test('b', true).test('_b', true);
        }).toString();


        return new Promise(function (res, rej) {
            return execSync(`node -e "(${sample}())"`, {
                encoding : 'utf8',
                cwd : __dirname
            });

        }).catch(function (err) {
            return err;
        }).then(function (command) {
            assert.strictEqual(command.output.join('\n'), [
                '',
                '- \u001b[32m(/)\u001b[0m A',
                '  - \u001b[31m(X)\u001b[0m a',
                '    - \u001b[31m(X)\u001b[0m _a',
                '- \u001b[32m(/)\u001b[0m B',
                '  - \u001b[32m(/)\u001b[0m b',
                '    - \u001b[32m(/)\u001b[0m _b',
                '\u001b[31mTest suite failed!\u001b[0m',
                '',
                ''
            ].join('\n'));
        });
    });
});
